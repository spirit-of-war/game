<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tower Defence</title>
    <script src="lib/phaser.min.js"></script>
    <script src="src/Player.js"></script>
    <script src="src/HUD.js"></script>
    <script src="src/hud/BuildMenu.js"></script>
    <script src="src/hud/Hummer.js"></script>
    <script src="src/hud/GamePausedBlock.js"></script>
    <script src="src/hud/FailedMenu.js"></script>
    <script src="src/hud/WinMenu.js"></script>
    <script src="src/hud/Board.js"></script>
    <script src="src/hud/TowerUpgradeMenu.js"></script>
    <script src="src/hud/TowerBuildMenu.js"></script>
    <script src="src/factory/TowerFactory.js"></script>
    <script src="src/factory/BuildMenuFactory.js"></script>
    <script src="src/Enemy.js"></script>
    <script src="src/towers/Tower.js"></script>
    <script src="src/towers/ArcherTower.js"></script>
    <script src="src/towers/FrostTower.js"></script>
    <script src="src/towers/FireTower.js"></script>
    <script src="src/bullets/Bullet.js"></script>
    <script src="src/bullets/Bomb.js"></script>
    <script src="src/bullets/Arrow.js"></script>
    <script src="src/HealthBar.js"></script>
    <script src="src/Level.js"></script>
    <script src="src/Global.js"></script>
    <script src="src/MainMenu.js"></script>
    <script src="src/LevelMenu.js"></script>
    <script src="src/Boot.js"></script>
    <script src="src/app.js"></script>
</head>
<body>
    <div id="game"></div>
</body>
</html>
