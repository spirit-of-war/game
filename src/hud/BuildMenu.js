var BuildMenu = function(x,y, group)
{
    this.menuCont = game.add.sprite(x, y, 'build-menu', null, group);
    this.menuCont.anchor.setTo(.5, .5);
    this.menuCont.inputEnabled = true;
    this.menuCont.scale = {x:.1, y: .1};
    this.group = group;

    var boardsCoord = [
        {x: -52, y: -36},
        {x: 50, y: -36},
        {x: 50, y: 48},
        {x: -52, y: 48}

    ];

    this.boards = this._createBoards(boardsCoord);
    this.hummer = this._createHummer();
    this.show();
};

BuildMenu.prototype.show = function()
{

    game.add.tween(this.menuCont.scale).to( { x: 1, y: 1 }, 300, Phaser.Easing.Quadratic.InOut, true);
    game.world.bringToTop(this.group);
};

BuildMenu.prototype.hide = function()
{
    game.add.tween(this.menuCont).to( { width: 0, height: 0 }, 300, Phaser.Easing.Quadratic.InOut, true)
        .onComplete.add(this.destroy, this);
};

BuildMenu.prototype.destroy = function()
{
    this.menuCont.destroy();
};

BuildMenu.prototype._createBoards = function(boardsCoord)
{
    var boards = [];
    for (var i=0; i< boardsCoord.length; i++) {
        var board = new Board(boardsCoord[i].x,boardsCoord[i].y);
        this.menuCont.addChild(board.sprite);
        boards.push(board);
    }

    return boards;
};

BuildMenu.prototype._createHummer = function() {
    var hummer = new Hummer(0,0);
    this.menuCont.addChild(hummer.sprite);

    return hummer;
};

BuildMenu.prototype.playHummerAnimation = function()
{
    return this.hummer.runAnimationBuild()
};

BuildMenu.prototype.getBoard = function(number)
{
    return this.boards[number-1];
};