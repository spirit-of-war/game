var GamePausedBlock = function (HUD)
{
    this.HUD = HUD;
    this.pausedBlock = this._draw();
    this.failedMenu = new FailedMenu(HUD);
    this.winMenu = new WinMenu(HUD);
};

GamePausedBlock.prototype._draw = function()
{
    var rect = this.HUD.level.add.graphics(0, 0);
    rect.lineStyle(2, 0x000000, 1);
    rect.beginFill(0x000000, 1);
    rect.drawRect(0, 0, this.HUD.level.world.width, this.HUD.level.world.height);
    rect.alpha = 0;
    rect.visible = false;

    return rect;
};

GamePausedBlock.prototype._showPausedBlock = function()
{
    this.HUD.level.add.tween(this.pausedBlock).to( { alpha: 0.7 }, 400, null, true, 0, 0, false);
    this.pausedBlock.visible = true;
    this.HUD.level.world.bringToTop(this.pausedBlock);
};

GamePausedBlock.prototype.hide = function()
{
    game.paused = false;
    this.HUD.level.add.tween(this.gamePausedBlock.rect).to( { alpha: 0 }, 500, null, true, 0, 0, false);
    this.gamePausedBlock.rect.visible = false;
    this.gamePausedBlock.textResult.visible = false;
};

GamePausedBlock.prototype.showFailedMenu = function()
{
    this._showPausedBlock();
    this.failedMenu.show();
};

GamePausedBlock.prototype.showWinMenu = function()
{
    this._showPausedBlock();
    this.winMenu.show();
};