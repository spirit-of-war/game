var TowerUpgradeMenu = function(x,y,group, tower)
{
    BuildMenu.apply(this,arguments);
    this.tower = tower;
    this.HUD = null;

    this.draw();
};

TowerUpgradeMenu.prototype = Object.create(BuildMenu.prototype);

TowerUpgradeMenu.prototype.draw = function()
{
    if(!this.tower.getNextUpgradeData()){
        return false;
    }
    this.getBoard(4)
        .addIcon('icon-arrows')
        .addCost(this.tower.getNextUpgradeData().goldAmount)
        .addHandler(this.callback4, this);
};

TowerUpgradeMenu.prototype.callback4 = function()
{
    if(!this.tower.isUpgradeProcess && this.tower.tryDeductGold()) {
        this.getBoard(4).inputEnabled = false;
        this.tower.isUpgradeProcess = true;
        this.tower.upgrade();
        this.HUD.hideTowerInfo();
        this.playHummerAnimation()
            .onComplete.add(function(){
            this.tower.isUpgradeProcess = false;
            this.tower.isSelected = false;
        }, this)
    }
};