var FailedMenu = function(HUD)
{
    this.HUD = HUD;

    this.menu = this._draw();
};

FailedMenu.prototype._draw = function()
{
    var style = {font: "bold 22px Arial", fill: "#ffffff",boundsAlignH: "center", boundsAlignV: "middle"};
    var titleText = this.HUD.level.add.text(0, 0, 'Вы проиграли', style);
    titleText.setTextBounds(0,46, 260, 0);

    var tableWidth = 320,
        tableHeight = 400;
    var table = this.HUD.level.add.sprite((this.HUD.level.world.width - tableWidth)/2, (this.HUD.level.world.height - tableHeight)/2, 'failed-menu-table');

    var menuHeader = this.HUD.level.add.sprite((tableWidth/2 - 104),-2, 'menu-header');
    menuHeader.scale = { x: 0.8, y: 0.8 };
    menuHeader.addChild(titleText);
    table.addChild(menuHeader);

    var window = this.HUD.level.add.sprite(38,86, 'failed-menu-wondow');
    table.addChild(window);

    var btnGroup = this.HUD.level.add.group();
    var btnLeft = this.HUD.level.add.sprite(50,326, 'failed-menu-btn-left', null, btnGroup);
    var btnRestart = this.HUD.level.add.sprite(178,326, 'failed-menu-btn-restart',null, btnGroup);
    btnGroup.setAll('inputEnabled',true);
    btnGroup.setAll('input.useHandCursor',true);
    table.addChild(btnLeft);
    table.addChild(btnRestart);

    btnLeft.events.onInputOver.add(function(){btnLeft.tint = 0xd5611f},this);
    btnLeft.events.onInputOut.add(function(){btnLeft.tint = 0xffffff;},this);
    btnRestart.events.onInputOver.add(function(){btnRestart.tint = 0xd5611f},this);
    btnRestart.events.onInputOut.add(function(){btnRestart.tint = 0xffffff;},this);

    table.visible = false;

    table.btnLeft = btnLeft;
    table.btnRestart = btnRestart;

    return table;
};

FailedMenu.prototype.show = function()
{
    this.menu.visible = true;
    this.HUD.level.world.bringToTop(this.menu);
};