var TowerBuildMenu = function(x,y,group)
{
    BuildMenu.apply(this,arguments);
    this.forTowerPointer = null;
    this.HUD = null;
    this.draw();

};

TowerBuildMenu.prototype = Object.create(BuildMenu.prototype);

TowerBuildMenu.prototype.draw = function()
{
    this.getBoard(4)
        .addIcon(Global.towersData.archerTower.spriteName)
        .addCost(Global.towersData.archerTower.goldAmount)
        .addHandler(this.buildArcherTower, this);
    this.getBoard(1)
        .addIcon(Global.towersData.frostTower.spriteName)
        .addCost(Global.towersData.frostTower.goldAmount)
        .addHandler(this.buildFrostTower, this);
    this.getBoard(2)
        .addIcon(Global.towersData.fireTower.spriteName)
        .addCost(Global.towersData.fireTower.goldAmount)
        .addHandler(this.buildFireTower, this);
};

TowerBuildMenu.prototype.buildArcherTower = function()
{
    this.tryBuildTower(Global.towersData.archerTower.spriteName);
};

TowerBuildMenu.prototype.buildFrostTower = function()
{
    this.tryBuildTower(Global.towersData.frostTower.spriteName);
};

TowerBuildMenu.prototype.buildFireTower = function()
{

     this.tryBuildTower(Global.towersData.fireTower.spriteName);

};

TowerBuildMenu.prototype.tryBuildTower = function(towerName)
{
    var towerGoldAmount = Global.towersData[towerName].goldAmount;
    if(this.HUD.player.deductGold(towerGoldAmount)) {
        this.HUD.towerNameForCreate = towerName;
        this.HUD.hideTowerInfo();
        var pointer = this.forTowerPointer;
        this.HUD.buildTower(pointer);
        this.HUD.level.setBuiltUpArea(pointer.x, pointer.y, false);
    }

    // console.log(this.HUD.level.player);
};