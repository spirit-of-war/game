var Hummer = function (x,y, anchor)
{
    anchor = anchor || {x:.5, y: .6};
    this.animationName = 'build';
    this.sprite = game.add.sprite(x, y, 'build-menu-hummer',7);
    this.sprite.animations.add(this.animationName, [0,1,2,3,4,5,6,7], 50 , false);

    this.sprite.anchor.setTo(anchor.x, anchor.y);
};

Hummer.prototype.runAnimationBuild = function()
{
    return this.sprite.animations.play(this.animationName);
};