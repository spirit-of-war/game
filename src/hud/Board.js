var Board = function(x,y, anchor)
{
    anchor = anchor || {x:.5, y: .5};
    this.sprite = game.add.sprite(x, y, 'build-menu-board');
    this.sprite.anchor.setTo(.5, .5);
    this.sprite.inputEnabled = true;
    this.sprite.input.useHandCursor = true;
    this.sprite.anchor.setTo(anchor.x, anchor.y);


};

Board.prototype.addHandler = function(callback, ctx)
{
    this.sprite.events.onInputDown.add(callback, ctx);
    this._addHoverTint();
    return this;
};

Board.prototype.addIcon = function(iconName)
{
    var icon = game.add.sprite(0, -8, iconName, 0);
    icon.scale = { x: 0.4, y: 0.4 };
    icon.anchor.setTo(.5, .5);
    this.sprite.addChild(icon);

    return this;
};

Board.prototype.addCost = function(cost)
{
    var style = {font: "bold 12px Arial", fill: "#fff",boundsAlignH: "center", boundsAlignV: "middle"};
    var textCost = game.add.text(-9, 7, null, style);
    textCost.setText(cost);
    this.sprite.addChild(textCost);

    return this;
};

Board.prototype._addHoverTint = function()
{
    var board = this.sprite;
    this.sprite.events.onInputOver.add(function(){board.tint = 0xd5611f},this);
    this.sprite.events.onInputOut.add(function(){board.tint = 0xffffff;},this);
};