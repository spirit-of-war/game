var BuildMenuFactory = function(HUD){
    this.HUD = HUD;
};

BuildMenuFactory.prototype.getTowerUpgrade = function(tower, group)
{
    var x = tower.tower.x + tower.tower.width/2,
        y = tower.worldY;
    var menu = new TowerUpgradeMenu(x, y, group, tower);
    menu.HUD = this.HUD;
    return menu;
};

BuildMenuFactory.prototype.getTowerBuild = function(tileCoord, group)
{
    var x = tileCoord.x + Global.TILE_SQUARE/2,
        y = tileCoord.y + Global.TILE_SQUARE/2;
    var menu = new TowerBuildMenu(x, y, group);
    menu.forTowerPointer = tileCoord;
    menu.HUD = this.HUD;
    return menu;
};