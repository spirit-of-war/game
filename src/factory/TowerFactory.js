var TowerFactory = function(level){
    this.level = level;
};

TowerFactory.prototype.buildArrowTower = function(position) {
    var params = {
        x: position.x,
        y: position.y,
        level: this.level,
        towerData: Global.towersData[Global.spriteName.ARCHER_TOWER]
    };
    this.level.player.deductGold(Global.towersData[Global.spriteName.ARCHER_TOWER].goldAmount);
    this.level.towers.push(new ArcherTower(params));

};

TowerFactory.prototype.buildFrostTower = function(position) {
    var params = {
        x: position.x,
        y: position.y,
        level: this.level,
        towerData: Global.towersData[Global.spriteName.FROST_TOWER]
    };
    this.level.towers.push(new FrostTower(params));
    this.level.player.deductGold(Global.towersData[Global.spriteName.FROST_TOWER].goldAmount);
};

TowerFactory.prototype.buildFireTower = function(position) {
    var params = {
        x: position.x,
        y: position.y,
        level: this.level,
        towerData: Global.towersData[Global.spriteName.FIRE_TOWER]
    };
    this.level.towers.push(new FireTower(params));
    this.level.player.deductGold(Global.towersData[Global.spriteName.FIRE_TOWER].goldAmount);
};