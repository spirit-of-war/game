var HUD = function (Level)
{
    this.player = Level.player;
    this.level = Level;
    this.isCreateTower = false;
    this.towerNameForCreate = false;
    this.towerFactory = new TowerFactory(this.level);
    this.buildMenuFactory = new BuildMenuFactory(this);
    this.gamePausedBlock = new GamePausedBlock(this);

    this.towerMenuGroup = Level.add.group();

    this.statistics = {};
    this.menuArr = [];

    this.init();

};

HUD.prototype.init = function()
{
    this.drawStatistics();
};
HUD.prototype.levelWin = function()
{
    this.gamePaused();
    ch.levelComplete(Global.curLevel);
    this.gamePausedBlock.showWinMenu();
};
HUD.prototype.levelLose = function()
{
    this.gamePaused();
    this.gamePausedBlock.showFailedMenu();
};

// HUD.prototype.gameEndPause = function()
// {
//     this.Drawer.hideResultScene();
//     this.gameUnpaused();
// };
HUD.prototype.gamePaused = function()
{
    // this.HUDGroup.setAll('inputEnabled', false);
    this.level.towerGroup.setAll('inputEnabled', false);
    this.level.time.events.add(Phaser.Timer.SECOND, function(){
        game.paused = true;
    }, this);
};
// HUD.prototype.gameUnpaused = function()
// {
//     this.HUDGroup.setAll('inputEnabled', true);
//     this.level.towerGroup.setAll('inputEnabled', true);
// };

HUD.prototype.drawStatistics = function()
{
    var textStyle = {font: "bold 14px Arial", fill: "#fff"};
    this.statistics.enemyCountText = this.level.add.text(20, 10, 'Врагов на карте: ', textStyle);
    this.statistics.waveIdxText = this.level.add.text(20, 30, 'Текущая волна: ', textStyle);
    this.statistics.nextWaveTimerText = this.level.add.text(20, 50, 'До появления мобов: ', textStyle);
    this.statistics.playerGold = this.level.add.text(20, 70, 'Золото: ', textStyle);
};

HUD.prototype.renderStatistics = function()
{
    this.statistics.enemyCountText.setText('Врагов на карте: ' + this.level.mobGroup.length);
    this.statistics.nextWaveTimerText.setText('До появления мобов: ' + Math.round(this.level.time.events.duration / Phaser.Timer.SECOND));
    this.statistics.waveIdxText.setText('Текущая волна: ' + this.level.curWave + ' из ' + this.level.waveData.length);
    this.statistics.playerGold.setText('Золото: ' + this.level.player.gold);
};

HUD.prototype.showNextWaveTimer = function()
{
    this.statistics.nextWaveTimerText.visible = true;
};

HUD.prototype.hideNextWaveTimer = function()
{
    this.statistics.nextWaveTimerText.visible = false;
};

HUD.prototype.buildTower = function(pointer) {
    if (this.towerNameForCreate === Global.spriteName.ARCHER_TOWER) {
        this.towerFactory.buildArrowTower(pointer);
    } else if (this.towerNameForCreate === Global.spriteName.FROST_TOWER) {
        this.towerFactory.buildFrostTower(pointer);
    } else if (this.towerNameForCreate === Global.spriteName.FIRE_TOWER) {
        this.towerFactory.buildFireTower(pointer);
    }
};

HUD.prototype.showTowerInfo = function(tower) {
     if (this.towerMenuGroup.length > 0) {
         this.hideTowerInfo();
    }
    this.level.towers.forEach(function(cTower){
        if(cTower !== tower) {
            cTower.isSelected = false;
        }
    });
    var menu = this.buildMenuFactory.getTowerUpgrade(tower,this.towerMenuGroup);
    this.menuArr.push(menu);
};

HUD.prototype.hideTowerInfo = function() {
    this.hideTowerMenu();
};

HUD.prototype.showBuildTowerMenu = function(tileCoord) {
    var menu = this.buildMenuFactory.getTowerBuild(tileCoord,this.towerMenuGroup);
    this.menuArr.push(menu);
    // this.Drawer.showBuildTowerMenu(tileCoord);
};

HUD.prototype.checkWin = function()
{
    if (this.level.mobGroup.length === 0 && this.level.isLastWave) {
        this.levelWin();
    }
};

HUD.prototype.checkLose = function()
{
    if(!this.player.checkLife()) {
        this.levelLose();
    }
};

// HUD.prototype.showTowerMenu = function(tower)
// {
//
// };

HUD.prototype.hideTowerMenu = function() {
    if(this.menuArr.length>0) {
        this.menuArr.pop().hide();
    }
};

HUD.prototype.restartLevel = function()
{
    var levelData = Global.levelData[Global.curLevel-1];
    game.state.add('Level', new Level(levelData));
    game.state.start('Level');
};

HUD.prototype.startLevelMenu = function()
{
    game.state.start('LevelMenu');
};

HUD.prototype.getFailedMenu = function()
{
    return this.gamePausedBlock.failedMenu.menu;
};

HUD.prototype.checkRestartLevelClick = function(pointer)
{
    if (this.getFailedMenu().btnRestart.getBounds().contains(pointer.x, pointer.y)) {
        game.paused = false;
        this.restartLevel();
    }
};

HUD.prototype.checkToMenuLevelClick = function(pointer)
{
    if(this.getFailedMenu().btnLeft.getBounds().contains(pointer.x, pointer.y)) {
        game.paused = false;
        this.startLevelMenu();
    }
};