var Drawer = function (Level, HUD)
{
    this.level = Level;
    this.HUD = HUD;
    this.primaryGraphics = this.level.add.graphics(0, 0);
    this.primaryGraphics.inputEnabled = true;
    // this.towerInfoBlock = {};
    this.gamePausedBlock = {};
    this.failedMenu = {};
    this.towerButtons = [];
    this.statistics = {};
    this.HUDGroup = this.level.add.group();
    this.HUDGroup.add(this.primaryGraphics);
    this.buildMenuFactory = new BuildMenuFactory(HUD);

    this.towerMenuGroup = Level.add.group();
    this.menuArr = [];


    this.draw();
};

Drawer.prototype.draw = function()
{
    // this.primaryGraphics.lineStyle(2, 0x7b5601, 1);
    // this.primaryGraphics.beginFill(0xc97bdc, 0.5);
    // this.primaryGraphics.drawRect(2, 0, 220, 100);

    // this.drawBuildingMenu();
    // this.drawTowerInfoBlock();
    // this.drawStatistics();
    // this.drawGamePausedBlock();
    // this.drawFailedMenu();
};

// Drawer.prototype.drawBuildingMenu = function()
// {
//     var towerMenuCont = this.level.add.graphics(4, 100);
//     var textArr = [];
//     towerMenuCont.lineStyle(2, 0xc6aa23, 1);
//     towerMenuCont.beginFill(0x606060, 1);
//     towerMenuCont.drawRect(0, 0, 216, 150);
//
//     this.HUDGroup.add(towerMenuCont);
//
//     var style = {font: "bold 14px Arial", fill: "#fff"};
//     var towersData = Global.towersData;
//
//     var buildArcherButton = this.HUDGroup.create(0, 0, Global.spriteName.ARCHER_TOWER, 0);
//     buildArcherButton.width = 32;
//     buildArcherButton.height = 32;
//     var buildFrostButton = this.HUDGroup.create(0, 0, Global.spriteName.FROST_TOWER, 0);
//     var buildFireButton = this.HUDGroup.create(0, 0, Global.spriteName.FIRE_TOWER, 0);
//     buildArcherButton.spriteName = Global.spriteName.ARCHER_TOWER;
//     buildFrostButton.spriteName = Global.spriteName.FROST_TOWER;
//     buildFireButton.spriteName = Global.spriteName.FIRE_TOWER;
//
//     textArr[0] = this.level.add.text(0, 0, towersData[Global.spriteName.ARCHER_TOWER].name, style);
//     textArr[1] = this.level.add.text(0, 0, 'Цена: ' + towersData[Global.spriteName.ARCHER_TOWER].goldAmount, style);
//     textArr[2] = this.level.add.text(0, 0, towersData[Global.spriteName.FROST_TOWER].name, style);
//     textArr[3] = this.level.add.text(0, 0, 'Цена: ' + towersData[Global.spriteName.FROST_TOWER].goldAmount, style);
//     textArr[4] = this.level.add.text(0, 0, towersData[Global.spriteName.FIRE_TOWER].name, style);
//     textArr[5] = this.level.add.text(0, 0, 'Цена: ' + towersData[Global.spriteName.FIRE_TOWER].goldAmount, style);
//
//     buildArcherButton.alignIn(towerMenuCont, null, -6, -6);
//     buildFireButton.alignIn(towerMenuCont, null, -6, -86);
//     buildFrostButton.alignIn(towerMenuCont, null, -6, -46);
//     textArr[0].alignIn(towerMenuCont, null, -46, -6);
//     textArr[1].alignIn(towerMenuCont, null, -46, -21);
//     textArr[2].alignIn(towerMenuCont, null, -46, -46);
//     textArr[3].alignIn(towerMenuCont, null, -46, -61);
//     textArr[4].alignIn(towerMenuCont, null, -46, -86);
//     textArr[5].alignIn(towerMenuCont, null, -46, -101);
//
//     this.towerButtons = [buildArcherButton,buildFrostButton,buildFireButton];
//     this.towerButtons.forEach(function(btn){
//         this.setButtonEvents(btn);
//     },this);
//
// };

// Drawer.prototype.setButtonEvents = function(btn){
//     btn.inputEnabled = true;
//     btn.alpha = 0.6;
//     btn.input.useHandCursor = true;
//     btn.events.onInputOver.add(this.btnHover);
//     btn.events.onInputOut.add(this.btnOut);
//     btn.events.onInputDown.add(this.btnToggle, this);
// };
// Drawer.prototype.btnHover = function(sprite) {
//     sprite.alpha = 1;
// };
// Drawer.prototype.btnOut = function(sprite) {
//     if(!!sprite.isActive === false) {
//         sprite.alpha = 0.6;
//     }
// };
// Drawer.prototype.btnToggle = function(sprite) {
//     this.towerButtons.forEach(function(btn) {
//         if (btn.spriteName === sprite.spriteName) {
//             return false;
//         }
//         btn.alpha = 0.6;
//         btn.isActive = false;
//     });
//     if(!!sprite.isActive === true) {
//         sprite.isActive = false;
//     } else {
//         sprite.isActive = true;
//     }
// };
// Drawer.prototype.drawTowerInfoBlock = function()
// {
//     var graphics = this.level.add.graphics(4, 270);
//     graphics.lineStyle(2, 0xc6aa23, 1);
//     graphics.beginFill(0x606060, 1);
//     this.towerInfoBlock.rect = graphics.drawRect(0, 0, 216, 120);
//     this.towerInfoBlock.rect.visible = false;
// };

// Drawer.prototype.showTowerInfoBlock = function(tower) {
//     var style = {font: "bold 12px Arial", fill: "#fff"};
    // this.towerInfoBlock.rect.visible = true;
    // this.towerInfoBlock.cont = this.level.add.sprite(0,0,null);
    // this.towerInfoBlock.rect.addChild(this.towerInfoBlock.cont);
    // var icon = this.level.add.sprite(6, 16, tower.tower.generateTexture());
    // icon.width = 32;
    // icon.height = 32;
    // var nameText = this.level.add.text(46, 12, 'Имя: ' + tower.name, style);
    // var attackText = this.level.add.text(46, 32, 'Атака: ' + tower.damageAmount, style);
    // var rangeText = this.level.add.text(46, 52, 'Радиус: ' + tower.range, style);
    // var attackTypeText = this.level.add.text(46, 72, 'Тип: ' + tower.attackTypeName, style);

    // this.towerInfoBlock.cont.addChild(icon);
    // this.towerInfoBlock.cont.addChild(nameText);
    // this.towerInfoBlock.cont.addChild(attackText);
    // this.towerInfoBlock.cont.addChild(rangeText);
    // this.towerInfoBlock.cont.addChild(attackTypeText);

// };

// Drawer.prototype.showTowerInfo = function(tower) {
//     this.level.towers.forEach(function(cTower){
//         if(cTower !== tower) {
//             cTower.isSelected = false;
//         }
//     });
//     // this.showTowerInfoBlock(tower);
//     this.showTowerMenu(tower);
// };
// Drawer.prototype.hideTowerInfo = function() {
//     // this.hideTowerInfoBlock();
//     this.hideTowerMenu();
//
// };
// Drawer.prototype.hideTowerInfoBlock = function() {
//     if(this.towerInfoBlock.rect.visible) {
//         this.towerInfoBlock.rect.visible = false;
//         this.towerInfoBlock.cont.destroy();
//     }
// };

// Drawer.prototype.drawStatistics = function()
// {
//     var textStyle = {font: "bold 14px Arial", fill: "#fff"};
//     this.statistics.enemyCountText = this.level.add.text(20, 10, 'Врагов на карте: ', textStyle);
//     this.statistics.waveIdxText = this.level.add.text(20, 30, 'Текущая волна: ', textStyle);
//     this.statistics.nextWaveTimerText = this.level.add.text(20, 50, 'До появления мобов: ', textStyle);
//     this.statistics.playerGold = this.level.add.text(20, 70, 'Золото: ', textStyle);
// };
//
// Drawer.prototype.renderStatistics = function()
// {
//     this.statistics.enemyCountText.setText('Врагов на карте: ' + this.level.mobGroup.length);
//     this.statistics.nextWaveTimerText.setText('До появления мобов: ' + Math.round(this.level.time.events.duration / Phaser.Timer.SECOND));
//     this.statistics.waveIdxText.setText('Текущая волна: ' + this.level.curWave + ' из ' + this.level.waveData.length);
//     this.statistics.playerGold.setText('Золото: ' + this.level.player.gold);
// };

// Drawer.prototype.drawGamePausedBlock = function()
// {
//     var style = {font: "bold 14px Arial", fill: "#fff",boundsAlignH: "center", boundsAlignV: "middle"};
//     this.gamePausedBlock.rect = this.level.add.graphics(0, 0);
//     this.gamePausedBlock.rect.lineStyle(2, 0x000000, 1);
//     this.gamePausedBlock.rect.beginFill(0x000000, 1);
//     this.gamePausedBlock.rect.drawRect(0, 0, this.level.world.width, this.level.world.height);
//     this.gamePausedBlock.rect.alpha = 0;
//     this.gamePausedBlock.textResult = this.level.add.text(0, 0, null, style);
//     this.gamePausedBlock.textResult.setTextBounds(0,-50, this.level.world.width, this.level.world.height);
//     this.gamePausedBlock.rect.visible = false;
//     this.gamePausedBlock.textResult.visible = false;
// };

// Drawer.prototype.showNextWaveTimer = function()
// {
//     this.statistics.nextWaveTimerText.visible = true;
// };
//
// Drawer.prototype.hideNextWaveTimer = function()
// {
//     this.statistics.nextWaveTimerText.visible = false;
// };

// Drawer.prototype.showResultScene = function(text)
// {
//     this.gamePausedBlock.textResult.setText(text);
//     this.level.add.tween(this.gamePausedBlock.rect).to( { alpha: 0.7 }, 400, null, true, 0, 0, false);
//     this.gamePausedBlock.rect.visible = true;
//     this.gamePausedBlock.textResult.visible = true;
//     this.level.world.bringToTop(this.gamePausedBlock.rect);
//     this.level.world.bringToTop(this.gamePausedBlock.textResult);
// };

// Drawer.prototype.showFailedMenu = function()
// {
//     this.failedMenu.visible = true;
//     this.level.world.bringToTop(this.failedMenu);
// };

// Drawer.prototype.hideResultScene = function()
// {
//     game.paused = false;
//     this.level.add.tween(this.gamePausedBlock.rect).to( { alpha: 0 }, 500, null, true, 0, 0, false);
//     this.gamePausedBlock.rect.visible = false;
//     this.gamePausedBlock.textResult.visible = false;
// };

// Drawer.prototype.showTowerMenu = function(tower)
// {
//     var menu = this.buildMenuFactory.getTowerUpgrade(tower,this.towerMenuGroup);
//     this.menuArr.push(menu);
// };

// Drawer.prototype.hideTowerMenu = function()
// {
//     if(this.menuArr.length>0) {
//         this.menuArr.pop().hide();
//     }
// };

// Drawer.prototype.showBuildTowerMenu = function(tileCoord)
// {
//     var menu = this.buildMenuFactory.getTowerBuild(tileCoord,this.towerMenuGroup);
//     this.menuArr.push(menu);
// };

// Drawer.prototype.drawFailedMenu = function()
// {
//     var style = {font: "bold 22px Arial", fill: "#ffffff",boundsAlignH: "center", boundsAlignV: "middle"};
//     var titleText = this.level.add.text(0, 0, 'Вы проиграли', style);
//     titleText.setTextBounds(0,46, 260, 0);
//
//     var tableWidth = 320,
//         tableHeight = 400;
//     var table = this.level.add.sprite((this.level.world.width - tableWidth)/2, (this.level.world.height - tableHeight)/2, 'failed-menu-table');
//
//     var menuHeader = this.level.add.sprite((tableWidth/2 - 104),-2, 'menu-header');
//     menuHeader.scale = { x: 0.8, y: 0.8 };
//     menuHeader.addChild(titleText);
//     table.addChild(menuHeader);
//
//     var window = this.level.add.sprite(38,86, 'failed-menu-wondow');
//     table.addChild(window);
//
//     var btnGroup = this.level.add.group();
//     var btnLeft = this.level.add.sprite(50,326, 'failed-menu-btn-left', null, btnGroup);
//     var btnRestart = this.level.add.sprite(178,326, 'failed-menu-btn-restart',null, btnGroup);
//     btnGroup.setAll('inputEnabled',true);
//     btnGroup.setAll('input.useHandCursor',true);
//     table.addChild(btnLeft);
//     table.addChild(btnRestart);
//
//     btnLeft.events.onInputOver.add(function(){btnLeft.tint = 0xd5611f},this);
//     btnLeft.events.onInputOut.add(function(){btnLeft.tint = 0xffffff;},this);
//     btnRestart.events.onInputOver.add(function(){btnRestart.tint = 0xd5611f},this);
//     btnRestart.events.onInputOut.add(function(){btnRestart.tint = 0xffffff;},this);
//
//     // btnRestart.events.onInputDown.add(function(){this.restartLevel()},this);
//     // btnLeft.events.onInputDown.add(function(){this.startLevelMenu()},this);
//
//     table.visible = false;
//
//     table.btnLeft = btnLeft;
//     table.btnRestart = btnRestart;
//     this.failedMenu = table;
//
// };