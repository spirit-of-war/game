var LevelMenu = function()
{
    this.levelCont = null;
    this.startY = null;
    this.endY = null;

    this.stateName = 'Level';
};

LevelMenu.prototype.create = function()
{
    this.add.sprite(0,0,'bg');

    var style = {font: "bold 22px Arial", fill: "#ffffff",boundsAlignH: "center", boundsAlignV: "middle"};
    var titleText = this.add.text(0, 0, 'Выберите уровень', style);
    titleText.setTextBounds(0,46, 260, 0);

    var levelContWidth = 650;
    var levelContHeight = 400;
    this.startY = (this.world.height - levelContHeight)/2 - 500;
    this.endY = (this.world.height - levelContHeight)/2;

    var levelCont = this.add.sprite((this.world.width - levelContWidth)/2, this.startY, 'level-menu-table');

    var menuHeader = this.add.sprite((levelCont.width/2 - 131),-8, 'menu-header');
    menuHeader.addChild(titleText);
    levelCont.addChild(menuHeader);
    var rope1 = this.add.sprite(120,-280, 'menu-rope');
    var rope2 = this.add.sprite(500,-280, 'menu-rope');
    levelCont.addChild(rope1);
    levelCont.addChild(rope2);

    var data = Global.levelData;
    var levelButtonGroup = this.add.group();
    for (var i = 0; i < data.length; i++) {
        var level = data[i].level;
        var indexX = i%4*150 + 50;
        var indexY = Math.floor(i/4) * 100 + 100;
        var levelBtnGraphic = this.add.sprite(indexX, indexY, 'level-menu-bttn-empty');
        levelCont.addChild(levelBtnGraphic);
        var levelBtnText = this.add.sprite(46, 16, 'level-menu-numbers', level);
        levelBtnGraphic.addChild(levelBtnText);
        var star = this.add.sprite(15, 60, 'level-menu-stars', ch.getLevelRate(level));
        levelBtnGraphic.addChild(star);
        console.log(this.checkActiveLevel(level));
        if(this.checkActiveLevel(level)) {
            this.activeLevelBehavior(levelBtnGraphic,data[i])
        } else {
            levelBtnGraphic.tint = 0x777777;
            levelBtnText.tint = 0x777777;
            star.tint = 0x777777;
        }
        // levelBtnGraphic.alignIn(levelCont, null, -indexX, -indexY);



    }

    this.add.tween(levelCont).to( { y: this.endY }, 500, Phaser.Easing.Quadratic.InOut, true);

    this.levelCont = levelCont;
};

LevelMenu.prototype.checkActiveLevel = function(levelNumb)
{
    return levelNumb <= ch._levelsCompleted+1;
};

LevelMenu.prototype.activeLevelBehavior = function(levelBtnGraphic, curData)
{
    levelBtnGraphic.inputEnabled = true;
    levelBtnGraphic.input.useHandCursor = true;
    levelBtnGraphic.events.onInputOver.add(function(btn){btn.tint = 0xd5611f},this);
    levelBtnGraphic.events.onInputOut.add(function(btn){btn.tint = 0xffffff},this);
    levelBtnGraphic.events.onInputDown.add(this.startLevel, this, null, curData);
};


LevelMenu.prototype.startLevel = function(lvlBtn, pointer, levelData)
{
    Global.curLevel = levelData.level;
    ch.curLevel++;
    this.state.add(this.stateName, new Level(levelData));
    this.add.tween(this.levelCont)
        .to( { y: this.startY }, 500, Phaser.Easing.Quadratic.InOut, true)
        .onComplete.add(this.stateStart,this);



};

LevelMenu.prototype.stateStart = function()
{
    this.state.start(this.stateName);
};