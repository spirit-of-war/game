var MainMenu = function()
{

};

MainMenu.prototype.create = function()
{
    var backGraphics = this.add.graphics(0, 0);
    backGraphics.lineStyle(10, 0x666666, 1);
    backGraphics.beginFill(0xffffff, 1);
    backGraphics.drawRect(0, 0, this.world.width, this.world.height);

    var menuGraphics = this.add.graphics(0, 0);
    menuGraphics.beginFill(0xffffff, 1);
    menuGraphics.drawRect(0, 0, 300, 300);

    menuGraphics.alignIn(this.world.bounds, Phaser.CENTER);

    button = game.add.button(0, 0, 'playButton', this.startGame, this);
    button.alignIn(menuGraphics, Phaser.CENTER);
};

MainMenu.prototype.startGame = function()
{
    this.state.start('LevelMenu');
};