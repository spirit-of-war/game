var Player = function () {
    this.basicLife = 10;
    this.life = 10;
    this.gold = 1000;
    this.stage = 1;
    this.HUDMode = false;

    var textStyle = {font: "bold 16px Arial", fill: "#000"};
    this.castleLifeBar = new HealthBar(game, {x: 120, y: 600, width: 220, height: 30, animationDuration: 200 });
    this.healthText = game.add.text(0, 0, this.life + '/'+ this.basicLife, textStyle);
    var centerBarX = this.castleLifeBar.config.width/2 - 15; // При изменении размера шрифта, последнюю цифру надо регулировать
    var centerBarY = this.castleLifeBar.config.height/2 - 10;
    this.healthText.alignIn(this.castleLifeBar.barSprite, null, -centerBarX,-centerBarY);
};

Player.prototype.getLife = function()
{
  return this.life;
};

Player.prototype.setLife = function(life)
{
    if(typeof(life) === 'number') {
        this.life = life;
    }
};

Player.prototype.damageLife = function()
{
    if (this.life > 0) {
        this.life--;
        var percent = this.life*100/this.basicLife;
        this.castleLifeBar.setPercent(percent);
        this.healthText.setText(this.life + '/'+ this.basicLife);
    }

};

Player.prototype.checkLife = function()
{
  return this.life > 0;
};

Player.prototype.addGold = function(amount)
{
    this.gold += amount;
    return true;
};

Player.prototype.deductGold = function(amount)
{
    if(this.gold >= amount) {
        this.gold -= amount;
        return true;
    }
    return false;
};