var Boot = function()
{

};

Boot.prototype.init = function() {
    game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
    if (!game.device.desktop) {
        game.scale.startFullScreen();
        console.log(game.device.desktop);
    }
};

Boot.prototype.preload = function () {
    var data = Global.levelData;
    for (var i = 0; i < data.length; i++) {
        var mapSource =  data[i].mapSource;
        var mapName =  data[i].mapName;
        var tileSetName = data[i].tileSetName;
        var tileSetSource = data[i].tileSetSource;
        this.load.tilemap(mapName, mapSource, null, Phaser.Tilemap.TILED_JSON);
        this.load.spritesheet(tileSetName, tileSetSource,128,128);
    }
    this.load.spritesheet('mobs', 'assets/Spacecharas.png', 32, 32);
    this.load.spritesheet('walker', 'assets/monster_sprites/walker.png', 67, 64);
    this.load.spritesheet('knight', 'assets/monster_sprites/knight.png', 67, 64);
    this.load.spritesheet('greenPig', 'assets/monster_sprites/green-pig.png', 67, 64);
    this.load.spritesheet('bigHead', 'assets/monster_sprites/big-head.png', 67, 70);
    this.load.spritesheet('flier', 'assets/monster_sprites/flyer.png', 75, 64);
    this.load.spritesheet('archers', 'assets/towers/archers/archers.png', 39, 32);
    this.load.spritesheet('crossbowmen', 'assets/towers/archers/crossbowmen.png', 46, 32);
    this.load.spritesheet('snipers', 'assets/towers/archers/snipers.png', 39, 32);
    this.load.spritesheet('playButton', 'assets/Play.png', 275, 194);
    this.load.spritesheet('build-menu', 'assets/hud/build-menu2.png', 256, 228);
    this.load.spritesheet('build-menu-board', 'assets/hud/build-menu-board.png', 56, 48);
    this.load.spritesheet('build-menu-hummer', 'assets/hud/hummer-sprite.png', 45, 50);
    this.load.spritesheet('menu-header', 'assets/hud/header.png', 262, 108);
    this.load.spritesheet('menu-rope', 'assets/hud/rope2.png', 30, 354);
    this.load.spritesheet('failed-menu-btn-left', 'assets/hud/failed-menu/button_left.png', 90, 89);
    this.load.spritesheet('failed-menu-btn-restart', 'assets/hud/failed-menu/button_restart.png', 90, 89);
    this.load.spritesheet('failed-menu-table', 'assets/hud/failed-menu/table.png', 320, 400);
    this.load.spritesheet('failed-menu-wondow', 'assets/hud/failed-menu/wondow.png', 240, 266);
    this.load.spritesheet('icon-damage', 'assets/icons/damage.png', 31, 32);
    this.load.spritesheet('icon-arrows', 'assets/icons/arrows.png', 64, 64);
    this.load.spritesheet('level-menu-table', 'assets/level_menu/table.png', 650, 422);
    this.load.spritesheet('level-menu-bttn-empty', 'assets/level_menu/btton_empty.png', 120, 119);
    this.load.spritesheet('level-menu-numbers', 'assets/level_menu/numb-sprite2.png', 23, 40);
    this.load.spritesheet('level-menu-stars', 'assets/level_menu/stars-sprite2.png', 89, 49);
    this.load.spritesheet(Global.spriteName.ARCHER_TOWER, 'assets/towers/towers/towerss.png', 96, 111);
    this.load.spritesheet(Global.spriteName.FROST_TOWER, 'assets/frost_tower.png', 32, 32);
    // this.load.spritesheet(Global.spriteName.FIRE_TOWER, 'assets/fire_tower2.png', 32, 32);
    this.load.spritesheet(Global.spriteName.FIRE_TOWER, 'assets/towers/towers/stone/stone-tower.png', 96, 116);
    this.load.spritesheet('b-1', 'assets/towers/towers/stone/stone-back-sprite.png', 90, 25);
    this.load.spritesheet('c-1', 'assets/towers/towers/stone/stone-front-sprite.png', 90, 34);
    // this.load.spritesheet(Global.spriteName.EXPLOSION, 'assets/explosion-spritesheet.png', 32, 32);
    this.load.spritesheet(Global.spriteName.ARROW, 'assets/bullets/arrow-shot.png', 31, 14);
    this.load.spritesheet(Global.spriteName.BULLET, 'assets/bullets/bullet.png', 34, 31);
    this.load.spritesheet(Global.spriteName.EXPLOSION, 'assets/bullets/explosion.png', 53, 50);
    this.load.spritesheet('bg', 'assets/hud/bg.jpg', 1138, 640);
};

Boot.prototype.create = function () {
    this.state.start('MainMenu');
};

var ch = new function()
{
    this.curLevel = 0;
    this._levelsCompleted = 0;
    this._levelCompleteRate = {};
    this.permissionRate = [1,2,3];
    this.permissionLevels = [];

    this.setLevelCompleteRate = function(level, rate) {
        if(!this.permissionRate.includes(rate)) {
            throw new Error('Попытка присвоить уровню несуществующий рейтинг');
        }
        if(!this._levelCompleteRate[level] || this._levelCompleteRate[level] < rate) { // если рейтинга на уровне еще нет
            this._levelCompleteRate[level] = rate;                                   // либо новый рейтинг выше текущего
        }
    };
    this.setLevelsCompleted = function(level) {
        if(!this.permissionLevels.includes(level)) {
            throw new Error('Попытка указать несуществующий уровень');
        }
        if(this._levelsCompleted < level) {
            this._levelsCompleted = level;
        }
    };
    this.getLevelRate = function(level) {
        return this._levelCompleteRate[level];
    };

    this.levelComplete = function(level)
    {
        // todo рейтинг заменить на нормальные данные
        this.setLevelCompleteRate(level,1);
        this.setLevelsCompleted(level);
    };

    this.init = function() {
        var data = Global.levelData;
        for (var i = 0; i < data.length; i++) {
            var level = data[i].level;
            this.permissionLevels.push(level);
            this._levelCompleteRate[level] = 0;
        }

    };

    this.init();
};