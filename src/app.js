var game = new Phaser.Game(896, 640, Phaser.Canvas, 'game');

game.state.add('boot', new Boot());
game.state.add('MainMenu', new MainMenu());
game.state.add('LevelMenu', new LevelMenu());
game.state.start('boot');