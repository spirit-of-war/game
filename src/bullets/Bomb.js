var Bomb = function(x,y, enemy, damageAmount, mobs)
{
    Bullet.apply(this,arguments);
    this.speed = 400;
    this.isHitting = false;
    this.mobs = mobs;
    var that = this;
    var enemyData = {
        x: this.enemy.centerBounds.getBounds().x,
        y: this.enemy.centerBounds.getBounds().y
    };
    var isDeadMobFly = false; // включается если моб уничтожен, а снаряд еще летит
    this.sprite.update = function(){

        if(that.enemy.sprite.exists) {
            enemyData.x = that.enemy.centerBounds.getBounds().x;
            enemyData.y = that.enemy.centerBounds.getBounds().y;
            that.fly(enemyData.x, enemyData.y);
        } else if(!isDeadMobFly) {
            that.fly(enemyData.x, enemyData.y);
            isDeadMobFly = true;
        }
        that.runAnimate();
        if(!isDeadMobFly && that.checkCenterHitting(that.enemy) || isDeadMobFly && that.checkEndPoint(enemyData.x,enemyData.y)) {
            that.detonate();
        }
    };
};

Bomb.prototype = Object.create(Bullet.prototype);

Bomb.prototype.getSpriteName = function()
{
    return Global.spriteName.BULLET;
};
Bomb.prototype.runAnimate = function()
{
    this.sprite.angle += 20;
};
Bomb.prototype.checkEndPoint = function(x,y)
{
    return this.sprite.getBounds().contains(x,y);
};
Bomb.prototype.detonate = function()
{
    this.stopFly();
    var explosion = game.add.sprite(this.sprite.x, this.sprite.y, Global.spriteName.EXPLOSION, 0);
    explosion.anchor.setTo(.5,.5);
    this.sprite.destroy();

    var explAnimate = explosion.animations.add('explose',[0,1,2,3,4,5,6],20,false,true);
    explosion.play('explose', 20, false);
    explAnimate.onComplete.add(function(){
        explosion.destroy();
    }, this);
    var that = this;
    var damagedEnemyIds = {}; // сюда собираем id врагов которых продамажило, чтоб избежать повторного поражения одним снарядом
    explosion.update = function(){
        that.mobs.forEach(function(enemy, idx){
            if(enemy.sprite.exists && that.checkBoundsHitting(enemy,explosion) && !damagedEnemyIds[idx]) {
                damagedEnemyIds[idx] = true;
                enemy.getDamage(that.damageAmount);
            }
        })
    }
};