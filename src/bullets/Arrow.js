var Arrow = function(x,y, enemy, damageAmount)
{
    Bullet.apply(this,arguments);

    this.speed = 1400;
    this.isHitting = false;
    var that = this;

    this.sprite.update = function(){
        if(!that.enemy.sprite.exists) {
            that.sprite.destroy();
            return false;
        }
        if(!that.isHitting){
            that.fly(that.enemy.sprite.x+4,that.enemy.sprite.y + that.enemy.sprite.height/2);
            that.rotate();
            if(that.checkCenterHitting(that.enemy)) {
                that.hit();
            }
        } else {
            // that.stuckMove();
        }
    };
};

Arrow.prototype = Object.create(Bullet.prototype);

Arrow.prototype.getSpriteName = function()
{
    return Global.spriteName.ARROW;
};

Arrow.prototype.rotate = function()
{
    var angle = game.physics.arcade.angleToXY(this.sprite, this.enemy.sprite.x+4, this.enemy.sprite.y + this.enemy.sprite.height/2);
    this.sprite.angle = Math.ceil(angle*57.2958);
};

Arrow.prototype.hit = function()
{
    this.isHitting = true;
    this.enemy.getDamage(this.damageAmount);
    this.stopFly();
    this.sprite.destroy();
};

//todo движение застрявшей во враге стрелы
Arrow.prototype.stuckMove = function()
{
    this._setPosition(this.enemy.sprite.x, this.enemy.sprite.y)
};

Arrow.prototype._setPosition = function(x,y)
{
    this.sprite.x = x+this.sprite.width/2-30;
    this.sprite.y = y+20;
};