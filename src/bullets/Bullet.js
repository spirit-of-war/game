var Bullet = function(x,y, enemy, damageAmount)
{
    this.sprite = game.add.sprite(x, y, this.getSpriteName());
    this.sprite.anchor.setTo(.5, .5);
    game.physics.enable(this.sprite, Phaser.Physics.ARCADE);
    this.sprite.enableBody = true;
    this.enemy = enemy;

    this.damageAmount = damageAmount;
};

Bullet.prototype.getSpriteName = function()
{
    throw new Error('Необходимо переопределить метод getSpriteName');
};

Bullet.prototype.fly = function(x,y)
{
    game.physics.arcade.moveToXY(this.sprite, x, y, this.speed);
};

Bullet.prototype.stopFly = function()
{
    this.sprite.body.velocity.x = 0;
    this.sprite.body.velocity.y = 0;
};

Bullet.prototype._checkIntersects = function(enemyBounds, objectBounds)
{
    return Phaser.Rectangle.intersects(enemyBounds, objectBounds);
};

Bullet.prototype.checkCenterHitting = function(enemy, object)
{
    object = object || this.sprite;
    var boundsA = enemy.centerBounds.getBounds();
    var boundsB = object.getBounds();

    return this._checkIntersects(boundsA,boundsB);
};

Bullet.prototype.checkBoundsHitting = function(enemy, object)
{
    object = object || this.sprite;
    var boundsA = enemy.bounds.getBounds();
    var boundsB = object.getBounds();

    return this._checkIntersects(boundsA,boundsB);
};