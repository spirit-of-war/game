var FrostTower = function(params)
{
    Tower.apply(this,arguments);
};

FrostTower.prototype = Object.create(Tower.prototype);

FrostTower.prototype.attackEnemy = function()
{
    var bullet = this.level.bullets.create(this.tower.x + Global.CENTER_TILE, this.tower.y + Global.CENTER_TILE, Global.spriteName.ARROW);
    bullet.lifespan = this.speedBullet/(this.range * Phaser.Timer.SECOND) * this.circleRange.radius;
    game.physics.arcade.moveToXY(bullet, this.enemy.sprite.x + Global.CENTER_TILE, this.enemy.sprite.y + Global.CENTER_TILE, this.speedBullet);
    if (this.slowEnemyFactor < 1) {
        this.enemy.slowDown(this.slowEnemyFactor);
    }
    this.hitting(this.enemy);
};