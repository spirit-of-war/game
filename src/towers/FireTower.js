var FireTower = function(params)
{
    this.backSprite = game.add.sprite(params.x, params.y+48, 'b-1', 0,params.level.towerGroup);
    Tower.apply(this,arguments);
    this.frontSprite = game.add.sprite(0, 65, 'c-1', 0,this.level.towerGroup);
    this.tower.addChild(this.frontSprite);
};

FireTower.prototype = Object.create(Tower.prototype);

FireTower.prototype.attackEnemy = function()
{
    this.playAttackAnimation();
    new Bomb(this.tower.x + Global.CENTER_TILE, this.tower.y + Global.CENTER_TILE, this.enemy, this.damageAmount, this.level.mobs);
};

FireTower.prototype.playAttackAnimation = function()
{
    game.add.tween(this.backSprite).to( { y: this.backSprite.y-50 }, 200, Phaser.Easing.Quintic.InOut, true,0,0,true);
    game.add.tween(this.frontSprite).to( { y: this.frontSprite.y-50 }, 200, Phaser.Easing.Quintic.InOut, true,0,0,true);
};

FireTower.prototype.upgrade = function()
{
    Tower.prototype.upgrade.apply(this, arguments);
    this.backSprite.frame++;
    this.frontSprite.frame++;
    if(this.upgradeLevel === 4) {
        this.backSprite.y += 11;
    }
};