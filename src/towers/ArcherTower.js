var ArcherTower = function(params)
{
    Tower.apply(this,arguments);

    this.archer = game.add.sprite(params.x+30, params.y-10, 'archers', 0, this.mobGroup);
    this.archer.anchor.x = 0.3;
    this.archer.animations.add('attack', [0,1,2,3,4,0], 20, false);
};

ArcherTower.prototype = Object.create(Tower.prototype);

ArcherTower.prototype.attackEnemy = function()
{
    var arrowX = 10;
    if(this.enemy.sprite.x < this.archer.x && this.archer.scale.x > 0) {
        this.archer.scale.x *= -1;
        arrowX *= -1;

    } else if (this.enemy.sprite.x > this.archer.x && this.archer.scale.x < 0) {
        this.archer.scale.x *= -1;
    }
    this.archer.animations.play('attack');
    var bullet = new Arrow(this.archer.x+arrowX, this.archer.y+10, this.enemy, this.damageAmount);

};

ArcherTower.prototype.upgrade = function()
{
    Tower.prototype.upgrade.apply(this, arguments);
    if(this.upgradeLevel === 6) {
        this.fireTime = 50;
    }
};