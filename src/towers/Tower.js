var Tower = function(params) {

    var towerData = params.towerData;
    this.level = params.level;
    this.player = params.level.player;
    this.upgradeData = towerData.upgradeData;
    this.upgradeLevel = 1;
    this.tower = game.add.sprite(params.x, params.y, towerData.spriteName, 0, this.level.towerGroup);
    this.tower.inputEnabled = true;
    this.tower.input.useHandCursor = true;
    this.tower.events.onInputDown.add(this.clickHandler, this);
    this.worldX = params.x;
    this.worldY = params.y;
    this.name = towerData.name;
    this.attackTypeName = towerData.attackTypeName;
    this.speedBullet = towerData.speedBullet;
    this.fireTime = towerData.fireTime;
    this.checkEnemyTime = 50;
    this.fireLastTime = game.time.now + this.fireTime;
    this.checkEnemyLastTime = game.time.now + this.checkEnemyTime;
    this.enemy = false;
    this.slowEnemyFactor = towerData.slowEnemyFactor;
    this.damageAmount = towerData.damageAmount;
    this.attackType = towerData.attackType;
    this.isUpgradeProcess = false;
    this.isSelected = false;

    this.range = towerData.range;
    this.circleRange = new Phaser.Circle(params.x + Global.CENTER_TILE, params.y + Global.CENTER_TILE, this.range * Global.TILE_SQUARE + Global.TILE_SQUARE);
    var graphics = game.add.graphics(0, 0);
    graphics.lineStyle(1, 0x00ff00, 1);
    graphics.beginFill(0x00b8ff, 0.15);
    this.circle = graphics.drawCircle(this.circleRange.x, this.circleRange.y, this.circleRange.diameter);
    this.circle.visible = false;

    this.mobGroup = game.add.group();
    this.mobGroup.enableBody = true;
    this.mobGroup.physicsBodyType = Phaser.Physics.ARCADE;

    var that = this;
    this.tower.update = function()
    {
        that.fire();
    }
};

Tower.prototype.fire = function() {
    if (game.time.now <= this.checkEnemyLastTime) {
        return false;
    }

    this.checkEnemyLastTime = game.time.now + this.checkEnemyTime;
    if (game.time.now <= this.fireLastTime) {
        return false;
    }
    this.findEnemy();
    if(this.enemy && game.time.now) {
        this.attackEnemy();
        this.fireLastTime = game.time.now + this.fireTime;
    }

};

// abstract function
Tower.prototype.attackEnemy = function()
{
    throw new Error('Необходимо переопределить метод attackEnemy');
};

Tower.prototype.findEnemy = function()
{
    if (!this.enemy    // Если нет зафиксированного врага
        || (this.enemy && !this.enemy.sprite.exists) // или враг есть, но убит
        || (this.enemy && this.enemy.sprite.exists && !this.circleRange.contains(this.enemy.sprite.x, this.enemy.sprite.y)) // или враг жив, но вне зоны досягаемости
    ) {
        this.enemy = false;
        this.level.mobs.forEach(function(enemy){
            var inBounds = this.circleRange.contains(enemy.sprite.x, enemy.sprite.y);
            if (inBounds && !this.enemy) {
                this.enemy = enemy;
            }
        }, this);
    }
};

Tower.prototype.upgrade = function()
{
    this.tower.frame++;
    this.upgradeLevel++;
    this.damageAmount = this.upgradeData[this.upgradeLevel].damageAmount;

};
Tower.prototype.tryDeductGold = function()
{
    var upgradeAmount = this.upgradeData[this.upgradeLevel+1].goldAmount;
    return this.player.deductGold(upgradeAmount);
};
Tower.prototype.getNextUpgradeData = function() {
    return this.upgradeData[this.upgradeLevel+1];
};

Tower.prototype.clickHandler = function()
{
    if (this.level.gameHUD.isCreateTower) {
        return false;
    }
    var thisTower = this;

    this.isSelected = !this.isSelected;

    if (this.isSelected) {
        this.level.gameHUD.showTowerInfo(thisTower);
    } else {
        this.level.gameHUD.hideTowerInfo();
    }
};