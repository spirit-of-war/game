var Level = function(levelData)
{
    this.levelData = levelData;
    this.waveData = levelData.waveData;
    this.path = levelData.path;
    this.map = null;
    this.layer1 = null;
    this.layer2 = null;
    this.canAddTower = true;
    this.isSpawnProcessing = false;
    this.curWave = 0;
    this.isLastWave = false;
    this.cursors = null;
    this.enemyCountText = null;
    this.waveIdxText = null;
    this.nextWaveTimerText = null;
    this.player = null;
    this.gameHUD = null;
    this.mobGroup = null;
    this.towerGroup = null;
    this.layerObjGroup = null;
    this.towers = [];
    this.mobs = [];
    this.bullets = null;
    this.splashBullets = null;
    this.explosions = null;
};

Level.prototype.create = function () {
    this.physics.startSystem(Phaser.Physics.ARCADE);

    this.map = this.add.tilemap(this.levelData.mapName);
    this.map.addTilesetImage('tiles', this.levelData.tileSetName);

    for(var i = 1; i <= this.levelData.layersQuantity; i++) {
        if(i === 1) {
            this.layer1 = this.map.createLayer('groundLayer1');
        } else {
            this.map.createLayer('groundLayer' + i);
        }
    }

    this.map.setCollisionByExclusion([], true, this.layer1);
    this.layer1.resizeWorld();

    this.player = new Player();
    this.gameHUD = new HUD(this);
    // console.log(this.gameHUD.getFailedMenu().btnLeft.getBounds());
    this.input.onDown.add(this.listener, this, null, this.gameHUD, this.player);

    this.initGroups();

    this.cursors = this.input.keyboard.createCursorKeys();

    // game.input.keyboard.addKeyCapture([
    //     Phaser.Keyboard.LEFT,
    //     Phaser.Keyboard.RIGHT,
    //     Phaser.Keyboard.UP,
    //     Phaser.Keyboard.DOWN,
    //     Phaser.Keyboard.SPACEBAR
    // ]);
};
Level.prototype.initGroups = function() {

    this.mobGroup = this.add.group();
    this.mobGroup.enableBody = true;
    this.mobGroup.physicsBodyType = Phaser.Physics.ARCADE;

    this.layerObjGroup = game.add.group();
    this.layerObjGroup.enableBody = true;
    this.layerObjGroup.add(this.mobGroup);

    this.map.createFromObjects('test', 35, 'tail_set_11', 34, true, false, this.layerObjGroup);
    this.map.createFromObjects('test', 47, 'tail_set_11', 46, true, false, this.layerObjGroup);
    this.map.createFromObjects('test', 28, 'tail_set_11', 27, true, false, this.layerObjGroup);

    this.towerGroup = this.add.group();

    this.bullets = this.add.group();
    this.bullets.enableBody = true;
    this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
    this.bullets.collideWorldBounds = true;

    this.splashBullets = this.add.group();
    this.splashBullets.enableBody = true;
    this.splashBullets.physicsBodyType = Phaser.Physics.ARCADE;

    this.explosions = this.add.group();
    this.explosions.enableBody = true;
    this.explosions.physicsBodyType = Phaser.Physics.ARCADE;
};
Level.prototype.update = function () {
    this.sortZDepth(this.layerObjGroup);
    this.spawnEnemyWave();
    // this.gameHUD.cursorModify();
    this.gameHUD.checkWin();
    this.gameHUD.checkLose();

    this.mobs.forEach(function (enemy, idx) {
        if(!enemy.sprite.exists) {
            this.mobs.splice(idx,1);
            return;
        }
        enemy.moveElmt();
        this.checkMobFinished(enemy);
    },this);

    // this.towers.forEach(function (Tower) {
    //     Tower.fire();
    // });

    // this.physics.arcade.overlap(this.bullets, this.mobGroup, this.collisionHandler, null, this);

};
Level.prototype.render = function () {
    this.gameHUD.renderStatistics();
};

Level.prototype.listener = function (pointer) {
    if(game.paused) {
        //@todo добавить проверку на isFailed
        this.gameHUD.checkRestartLevelClick(pointer);
        this.gameHUD.checkToMenuLevelClick(pointer);
    } else {
        if(this.isBuiltUpArea(pointer.x, pointer.y)) {
            this.gameHUD.hideTowerMenu();
            this.gameHUD.showBuildTowerMenu(this.getPixelXY(pointer.x , pointer.y));
        }
    }
};
// Level.prototype.collisionHandler = function (bullet, enemy) {
//     enemy.alpha = 0.3;
//     this.time.events.add(50, function(enemys){enemys.alpha = 1;}, this, enemy);
//     bullet.kill();
// };


Level.prototype.spawnEnemyWave = function () {
    if (this.mobGroup.length > 0 || this.isSpawnProcessing || (this.curWave >= this.waveData.length)) {
        return false;
    }
    this.isSpawnProcessing = true;
    this.gameHUD.showNextWaveTimer();
    this.time.events.add(Phaser.Timer.SECOND * 4, this.generateEnemy, this, this.waveData[this.curWave], this.path);

    this.curWave++;
};

Level.prototype.generateEnemy = function (curWaveData, path) {
    var i = 0;

    this.gameHUD.hideNextWaveTimer();

    var count = curWaveData.count;
    var ctx = this;
    var enemysBcl = setInterval(function () {
        if (i < count && !game.paused) {
            var enemy = new Enemy(curWaveData, path, ctx.mobGroup, ctx.layerObjGroup);
            enemy.sprite.idx = ctx.mobs.length;
            ctx.mobs.push(enemy);
        } else {
            ctx.isSpawnProcessing = false;
            if(ctx.curWave === ctx.waveData.length) {
                ctx.isLastWave = true;
            }
            clearTimeout(enemysBcl);
        }
        i++;
    }, 500);
};

Level.prototype.checkMobFinished = function (enemy) {
    if(enemy.isFinished){
        enemy.destroySelf();
        this.player.damageLife();
    }
};

Level.prototype.sortZDepth = function (group) {
    group.sort('y', Phaser.Group.SORT_ASCENDING);
};


/**
 * Получить координаты тайла в сетке по координатам на экране
 * @param x
 * @param y
 * @returns {{x: *, y: *}}
 */
Level.prototype.getTileXY = function (x, y) {
    return {
        x: this.layer1.getTileX(x),
        y: this.layer1.getTileY(y)
    }
};

/**
 * Получить пиксельные координаты тайла по координатам на экране
 * @param x
 * @param y
 * @returns {{x: number, y: number}}
 */
Level.prototype.getPixelXY = function (x, y) {
    var tileXY = this.getTileXY(x, y);
    return {
        x: tileXY.x * Global.TILE_SQUARE,
        y: tileXY.y * Global.TILE_SQUARE
    };
};

/**
 * Получить все тайлы по координатам в сетке
 * 0 - самый нижний слой
 * @param tileX
 * @param tileY
 * @returns {Array}
 */
Level.prototype.getTiles = function (tileX, tileY) {
    var tile, tiles = [], layerIndex;
    for(var i = this.map.layers.length-1; i >= 0; i--) {
        layerIndex = this.map.getLayerIndex(this.map.layers[i].name);
        tile = this.map.getTile(tileX, tileY, layerIndex);
        if(tile) {
            tiles[layerIndex] = tile;
        }
    }
    return tiles;
};

/**
 * Можно ли строить на тайле верхнего слоя карты
 * передаются пиксельные координаты на экране
 * @param x
 * @param y
 * @returns {boolean}
 */
Level.prototype.isBuiltUpArea = function (x, y) {
    var tileXY = this.getTileXY(x, y),
        tiles = this.getTiles(tileXY.x, tileXY.y),
        tileProps, isBuiltUpArea = false;
    if(tiles && tiles.length) {
        tileProps = tiles[tiles.length-1].properties;
        isBuiltUpArea = tileProps && !!tileProps.isBuiltUpArea;
    }
    return isBuiltUpArea;
};

/**
 * Изменить признак доступности стройки на тайле верхнего слоя карты
 * передаются пиксельные координаты на экране
 * @param x
 * @param y
 * @param value
 */
Level.prototype.setBuiltUpArea = function (x, y, value) {
    var tileXY = this.getTileXY(x, y),
        tiles = this.getTiles(tileXY.x, tileXY.y),
        tileProps;
    if(tiles && tiles.length) {
        tileProps = tiles[tiles.length-1].properties;
        tileProps.isBuiltUpArea = !!value;
    }
};