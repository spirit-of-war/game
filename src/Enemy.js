var Enemy = function(waveData, path, group, layerGroup){
    var x = path[0].x * Global.TILE_SQUARE;
    var y = path[0].y * Global.TILE_SQUARE;
    var animData = Global.animationsData[waveData.mobName];
    var speed = waveData.speed;
    var health = waveData.health;
    var bounty = waveData.bounty;

    this.sprite = game.add.sprite(x, y, animData.name, animData.startFrame, group);
    // console.log(this.sprite.getBounds());
    this.sprite.anchor.x = animData.anchorX;
    this.sprite.health = health;

    this.sprite.animations.add('walk', animData.walk, 20 , true);
    this.sprite.animations.add('run', animData.run, 20 , true);
    this.sprite.animations.add('die', animData.die, 50 , false);

    this.layerGroup = layerGroup;
    this.speed = speed;
    this.animData = animData;
    this.speedX = 0;
    this.speedY = 0;
    this.currSpeedX = 0;
    this.currSpeedY = 0;
    this.curTile = 0;
    this.basicHealth = health;
    this.healtBar = new HealthBar(game, {x: x, y: y-3, width: 22, height: 3, animationDuration: 50 ,bar: {color: '#70D670'} });


    this.speedFactor = 1;
    this.baseSpeedFactor = 1;
    this.bounty = bounty;
    this.path = path;
    this.isFinished = false;

    this.bounds = backGraphics = game.add.graphics(0, 0);
    this.bounds.drawRect(animData.bounds.x, animData.bounds.y, animData.bounds.w, animData.bounds.h);
    this.centerBounds = game.add.graphics(0, 0);
    this.centerBounds.drawRect(animData.centerBounds.x, animData.centerBounds.y, 8, 8);
    this.sprite.addChild(this.bounds);
    this.sprite.addChild(this.centerBounds);
    this.sprite.animations.play(speed > 2 ? 'run' : 'walk');

    this.nextTile();
    this.moveElmt();

};

Enemy.prototype.slowDown = function (factor) {
    var thisEnemy = this;
    if (factor >= 0 && this.baseSpeedFactor === this.speedFactor) {
        var basicTint = this.sprite.tint;
        this.sprite.tint = 0x205c86;
        this.speedFactor = factor;
        this.speedX = this.speedX * this.speedFactor;
        this.speedY = this.speedY * this.speedFactor;
        setTimeout(function(){
            thisEnemy.sprite.tint = basicTint;
            thisEnemy.speedFactor = thisEnemy.baseSpeedFactor;
            thisEnemy.speedX = thisEnemy.currSpeedX * thisEnemy.speedFactor;
            thisEnemy.speedY = thisEnemy.currSpeedY * thisEnemy.speedFactor;
        }, 2000)
    }

};

Enemy.prototype.getDamage = function(damageAmount)
{
    this.sprite.damage(damageAmount);

    var percent = this.sprite.health*100/this.basicHealth;
    this.healtBar.setPercent(percent);
    if(this.sprite.health <= 0) {
        this.destroySelf();
        // this.level.player.addGold(this.bounty); // убрать это отсюда нахер! что вообще игрок забыл внутри класса башни? О_о
    } else {
        // this.sprite.alpha = 0.3;
        // game.time.events.add(50, function(enemy){enemy.alpha = 1;}, this, this.sprite);
    }
};
Enemy.prototype.destroySelf = function () {
    var animData = this.animData;
    var dying = game.add.sprite(this.sprite.x, this.sprite.y, animData.name, animData.startFrame, this.layerGroup);
    var die = dying.animations.add('die', animData.die, 30 , false);
    dying.anchor.x = animData.anchorX;
    if(this.sprite.scale.x < 0) {
        dying.scale.x *= -1;
    }
    this.sprite.destroy();
    this.healtBar.kill();
    die.onComplete.add(function(){
        dying.destroy();
    }, this);
    dying.animations.play('die');
};

Enemy.prototype.moveElmt = function () {
    this.sprite.x += this.speedX;
    this.sprite.y += this.speedY;

    // console.log('x: ' + enemy.x + ' y: ' + enemy.y);
    // if(enemy.speedX > 0 && enemy.x > enemy.next_positX-60) {
    //     enemy.y += 2;
    // } else if (enemy.speedX < 0 && enemy.x < enemy.next_positX+60) {
    //     enemy.y += 2;
    // } else if(enemy.speedY > 0 && enemy.y >= enemy.next_positY-60) {
    //     enemy.x += 2;
    // } else if (enemy.speedY < 0 && enemy.y <= enemy.next_positY-60) {
    //     enemy.x += 2;
    // }
    if (this.speedX > 0 && this.sprite.x >= this.next_positX
    || this.speedX < 0 && this.sprite.x <= this.next_positX
    || this.speedY > 0 && this.sprite.y >= this.next_positY
    || this.speedY < 0 && this.sprite.y <= this.next_positY) {

        this.nextTile();
    }

    // if (enemy.speedX > 0 && enemy.x > enemy.next_positX
    // || enemy.speedX < 0 && enemy.x < enemy.next_positX) {
    //     enemy.y = enemy.next_positY;
    //     enemy.speedY = 0;
    // }

    this.healtBar.setPosition(this.sprite.x, this.sprite.y);

};

Enemy.prototype.nextTile = function () {
    this.curTile++;
    if(!!this.path[this.curTile] == false){
        this.isFinished = true;
        return;
    }

    var speedX, speedY , animationName;
    this.next_positX = parseInt(this.path[this.curTile].x * Global.TILE_SQUARE+64);
    this.next_positY = parseInt(this.path[this.curTile].y * Global.TILE_SQUARE);
    // alert('x: ' + enemy.next_positX + ' y: ' + enemy.next_positY);

    if (this.next_positX > this.sprite.x) {
        if(this.sprite.scale.x < 0) {
            this.sprite.scale.x *= -1;
        }
        // animationName = 'right';
        speedX = this.speed;
    } else if (this.next_positX < this.sprite.x) {
        if(this.sprite.scale.x > 0) {
            this.sprite.scale.x *= -1;
        }
        speedX = -this.speed;
        // animationName = 'left';
    } else {
        speedX = 0;
    }

    if (this.next_positY > this.sprite.y) {
        // animationName = 'down';
        speedY = this.speed;
    } else if (this.next_positY < this.sprite.y) {
        // animationName = 'top';
        speedY = -this.speed;
    } else {
        speedY = 0;
    }

    // enemy.animations.play(animationName);
    this.currSpeedX = speedX;
    this.currSpeedY = speedY;
    this.speedX = speedX * this.speedFactor;
    this.speedY = speedY * this.speedFactor;
};