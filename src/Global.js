var Global = {
    TILE_SQUARE: 128,
    CENTER_TILE: 64,
    curLevel: 0,
    player: null
};
Global.animationsData = {
    // knight: {
    //     name: 'knight',
    //     down: [48, 49, 50],
    //     left: [60, 61, 62],
    //     right: [72, 73, 74],
    //     top: [84, 85, 86],
    //     startFrame: 49
    // },
    lion: {
        name: 'lion',
        down: [51, 52, 53],
        left: [63, 64, 65],
        right: [75, 76, 77],
        top: [87, 88, 89],
        startFrame: 52
    },
    hedgehog: {
        name: 'hedgehog',
        down: [54, 55, 56],
        left: [66, 67, 68],
        right: [78, 79, 80],
        top: [90, 91, 92],
        startFrame: 55
    },
    cosmo: {
        name: 'cosmo',
        down: [57, 58, 59],
        left: [69, 70, 71],
        right: [81, 82, 83],
        top: [93, 94, 95],
        startFrame: 58
    },
    walker: {
        name: 'walker',
        walk: [0,1,2,3,4,5,6,7,8,9],
        die: [10,11,12,13,14,15,16,17,18,19,19,19,19,19,19],
        run: [20,21,22,23,24,25,26,27,28,29],
        startFrame: [1],
        anchorX: 0.5,
        centerBounds: {
            x: -4,
            y: 30
        },
        bounds: {
            x: -10,
            y: 6,
            w: 20,
            h: 50
        }
    },
    knight: {
        name: 'knight',
        walk: [0,1,2,3,4,5,6,7,8,9],
        die: [10,11,12,13,14,15,16,17,18,19,19,19,19,19,19],
        run: [20,21,22,23,24,25,26,27,28,29],
        startFrame: [1],
        anchorX: 0.3,
        centerBounds: {
            x: -4,
            y: 30
        },
        bounds: {
            x: -12,
            y: 10,
            w: 20,
            h: 50
        }
    },
    greenPig: {
        name: 'greenPig',
        walk: [0,1,2,3,4,5,6,7,8,9],
        die: [10,11,12,13,14,15,16,17,18,19,19,19,19,19,19],
        run: [20,21,22,23,24,25,26,27,28,29],
        startFrame: [1],
        anchorX: 0.3,
        centerBounds: {
            x: -2,
            y: 30
        },
        bounds: {
            x: -12,
            y: 6,
            w: 24,
            h: 50
        }
    },
    bigHead: {
        name: 'bigHead',
        walk: [0,1,2,3,4,5,6,7,8,9],
        die: [10,11,12,13,14,15,16,17,18,19,19,19,19,19,19],
        run: [20,21,22,23,24,25,26,27,28,29],
        startFrame: [1],
        anchorX: 0.3,
        centerBounds: {
            x: -2,
            y: 38
        },
        bounds: {
            x: -12,
            y: 10,
            w: 24,
            h: 52
        }
    },
    flier: {
        name: 'flier',
        walk: [0,1,2,3,4,5,6,7,8,9],
        die: [10,11,12,13,14,15,16,17,18,19,19,19,19,19,19],
        run: [20,21,22,23,24,25,26,27,28,29],
        startFrame: [1],
        anchorX: 0.4,
        centerBounds: {
            x: -2,
            y: 20
        },
        bounds: {
            x: -22,
            y: 8,
            w: 46,
            h: 30
        }
    }
};
Global.spriteName = {
    ARCHER_TOWER: 'archerTower',
    FROST_TOWER: 'frostTower',
    FIRE_TOWER: 'fireTower',
    EXPLOSION: 'explosion',
    BULLET: 'bullet',
    ARROW: 'arrow'
};
Global.towersData = {
    archerTower: {
        spriteName: Global.spriteName.ARCHER_TOWER,
        name: 'Башня лучников',
        attackType: 'archer',
        attackTypeName: 'стрелы',
        speedBullet: 2000,
        fireTime: 1000,
        slowEnemyFactor: 1,
        damageAmount: 6,
        range: 6,
        goldAmount: 80,
        upgradeData: {
            2 : {
                damageAmount: 9,
                goldAmount: 40,
            },
            3 : {
                damageAmount: 12,
                goldAmount: 40,
            },
            4 : {
                damageAmount: 15,
                goldAmount: 30,
            },
            5 : {
                damageAmount: 18,
                goldAmount: 50,
            },
            6 : {
                damageAmount: 1,
                goldAmount: 60,
            }
        }
    },
    frostTower: {
        spriteName: Global.spriteName.FROST_TOWER,
        name: 'Башня холода',
        attackType: 'frost',
        attackTypeName: 'замораживает',
        speedBullet: 2000,
        fireTime: 1000,
        slowEnemyFactor: 0.6,
        damageAmount: 2,
        range: 4,
        goldAmount: 130
    },
    fireTower: {
        spriteName: Global.spriteName.FIRE_TOWER,
        name: 'Башня огня',
        attackType: 'splash',
        attackTypeName: 'атака по области',
        speedBullet: 600,
        fireTime: 1000,
        slowEnemyFactor: 1,
        damageAmount: 3,
        range: 4,
        goldAmount: 170,
        upgradeData: {
            2 : {
                damageAmount: 9,
                goldAmount: 40,
            },
            3 : {
                damageAmount: 12,
                goldAmount: 40,
            },
            4 : {
                damageAmount: 15,
                goldAmount: 30,
            },
            5 : {
                damageAmount: 18,
                goldAmount: 50,
            },
            6 : {
                damageAmount: 1,
                goldAmount: 60,
            },
            7 : {
                damageAmount: 15,
                goldAmount: 30,
            },
            8 : {
                damageAmount: 18,
                goldAmount: 50,
            },
            9 : {
                damageAmount: 100,
                goldAmount: 60,
            }
        }
    }
};
Global.levelData = [
    {
        level: 1,
        mapSource: 'assets/releaseMap.json',
        mapName: 'map2',
        tileSetName: 'tail_set_11',
        tileSetSource: 'assets/tail_set_11.png',
        layersQuantity: 8,
        path: [
            {x: 7, y: 0},
            {x: 2, y: 0},
            {x: 2, y: 1},
            {x: 3, y: 1},
            {x: 3, y: 2},
            {x: 6, y: 2},
            {x: 6, y: 4},
            {x: 4, y: 4},
            {x: 4, y: 5}
        ],
        waveData: [
            {
                mobName: Global.animationsData.walker.name,
                count: 10,
                speed: 4,
                health: 20,
                bounty: 50,
                basicAnimation: 'run'
            }
        ]
    },
    {
        level: 2,
        mapSource: 'assets/releaseMap.json',
        mapName: 'map2',
        tileSetName: 'tail_set_11',
        tileSetSource: 'assets/tail_set_11.png',
        layersQuantity: 8,
        path: [
            {x: 7, y: 0},
            {x: 2, y: 0},
            {x: 2, y: 1},
            {x: 3, y: 1},
            {x: 3, y: 2},
            {x: 6, y: 2},
            {x: 6, y: 4},
            {x: 4, y: 4},
            {x: 4, y: 5}
        ],
        waveData: [
            {
                mobName: Global.animationsData.walker.name,
                count: 10,
                speed: 4,
                health: 20,
                bounty: 50,
                basicAnimation: 'run'
            },
            {
                mobName: Global.animationsData.knight.name,
                count: 7,
                speed: 2,
                health: 15,
                bounty: 60,
                basicAnimation: 'walk'
            },
            {
                mobName: Global.animationsData.greenPig.name,
                count: 8,
                speed: 2,
                health: 20,
                bounty: 70,
                basicAnimation: 'walk'
            },
            {
                mobName: Global.animationsData.bigHead.name,
                count: 4,
                speed: 2,
                health: 45,
                bounty: 150,
                basicAnimation: 'walk'
            },
            {
                mobName: Global.animationsData.flier.name,
                count: 6,
                speed: 3,
                health: 30,
                bounty: 90,
                basicAnimation: 'run'
            },
            {
                mobName: Global.animationsData.knight.name,
                count: 15,
                speed: 2,
                health: 40,
                bounty: 100,
                basicAnimation: 'walk'
            },
            {
                mobName: Global.animationsData.bigHead.name,
                count: 10,
                speed: 2,
                health: 450,
                bounty: 400,
                basicAnimation: 'walk'
            }
        ]
    }
];